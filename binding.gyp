{
    "targets": [
        {
            "target_name": "addon",
            "cflags_cc": [ 
                "-std=c++11",
                "-fexceptions"
            ],
            "sources": [ 
                "<!@(ls -1 src/*.cpp)"
            ],
            'include_dirs': [
                "<!@(node -p \"require('node-addon-api').include\")"
            ],
            'libraries': [
                "-lboost_system",
                "-lboost_filesystem"
            ],
            'dependencies': [
                "<!(node -p \"require('node-addon-api').gyp\")"
            ],
            'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ]
        }
    ]
}
