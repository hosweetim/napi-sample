const addOnWithoutBindings = require('./build/Release/addon.node');
console.log(addOnWithoutBindings);

const addon = require('bindings')('addon');
console.log(addon);

console.log(addon.hello())
console.log(addon.addition(1, 2))
console.log(addon.isSame([true, true, false]))
console.log(addon.isSame([true, true, false, false, true]))
console.log(addon.isSame([true, true, true, true, true, true]))
console.log(addon.isSame([false, false, false, false, false, false]))
console.log('total', addon.total([1, 2, 3]));
console.log('total', addon.total([true, true, true, true, true]));
console.log('interpolator', addon.interpolator([1, 2, 3]));
console.log('createLaneNode', addon.createLaneNode('hello world!', 1, 20))
console.log('generateLaneNodes', addon.generateLaneNodes([
    { x: 1, y: 10 },
    { x: 2, y: 20 },
    { x: 2, z: 3 },
]))

const util = new addon.Util(100);
console.log(util.getValue())
util.increment()
console.log(util.getValue())

console.log(addon.getBoostVersion());

module.exports = addon;
