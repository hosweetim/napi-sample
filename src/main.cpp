#include <napi.h>

#include "sample.h"
#include "utilWrapper.h"

Napi::Object InitAll(Napi::Env env, Napi::Object exports)
{
    sample::Init(env, exports);
    return UtilWrapper::Init(env, exports);
}

NODE_API_MODULE(addon, InitAll)