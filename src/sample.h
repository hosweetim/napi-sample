#ifndef SAMPLE_H
#define SAMPLE_H

#include <string>
#include <vector>

#include <napi.h>

struct LaneNode {
    std::string name;
    int index;
    double x;
    double y;
    double maxSpeed_mps;
};

namespace sample {
    std::string hello();
    Napi::String HelloWrapped(const Napi::CallbackInfo& info);

    int addition(int a, int b);
    Napi::Number AdditionWrapped(const Napi::CallbackInfo& info);

    int total(const std::vector<int>& data);
    Napi::Number TotalWrapped(const Napi::CallbackInfo& info);

    bool isSame(const std::vector<bool>& data);
    Napi::Boolean IsSameWrapped(const Napi::CallbackInfo& info);

    std::vector<double> interpolator(const std::vector<double>& data);
    Napi::Array InterpolatorWrapped(const Napi::CallbackInfo& info);

    LaneNode createLaneNode(const std::string& name, double x, double y);
    Napi::Object CreateLaneNodeWrapped(const Napi::CallbackInfo& info);

    std::vector<LaneNode> generateLaneNodes(std::vector<std::pair<double, double>> nodes);
    Napi::Array GenerateLaneNodesWrapped(const Napi::CallbackInfo& info);

    std::string getBoostVersion();
    Napi::String GetBoostVersionWrapped(const Napi::CallbackInfo& info);

    Napi::Object Init(Napi::Env env, Napi::Object exports);
}

#endif // SAMPLE_H