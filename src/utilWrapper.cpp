#include "utilWrapper.h"

Napi::FunctionReference UtilWrapper::constructor;

Napi::Object UtilWrapper::Init(Napi::Env env, Napi::Object exports)
{
    Napi::HandleScope scope(env);

    Napi::Function func = DefineClass(env, "Util", {
        InstanceMethod("getValue", &UtilWrapper::GetValue),
        InstanceMethod("increment", &UtilWrapper::Increment),
    });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("Util", func);
    return exports;
}

UtilWrapper::UtilWrapper(const Napi::CallbackInfo& info): Napi::ObjectWrap<UtilWrapper>(info)
{
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    int length = info.Length();

    if (length != 1) {
        Napi::TypeError::New(env, "Only one argument expected").ThrowAsJavaScriptException();
    }

    if (!info[0].IsNumber()) {
        Napi::Object objectParent = info[0].As<Napi::Object>();
        UtilWrapper *utilWrapper = Napi::ObjectWrap<UtilWrapper>::Unwrap(objectParent);
        Util *util = utilWrapper->GetInternalInstance();
        this->util = util;
        return;
    }

    Napi::Number value = info[0].As<Napi::Number>();
    this->util = new Util(value.Int32Value());
}

Napi::Value UtilWrapper::GetValue(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    Napi::EscapableHandleScope scope(env);

    int value = this->util->getValue();

    return scope.Escape(Napi::Number::New(info.Env(), value));
}

void UtilWrapper::Increment(const Napi::CallbackInfo& info)
{
    this->util->increment();
}

Util* UtilWrapper::GetInternalInstance()
{
    return this->util;
}