#ifndef UTIL_HPP
#define UTIL_HPP

class Util {
public:
    Util(int value);
    int getValue();
    void increment();

private:
    int value;
};

#endif // UTIL_HPP