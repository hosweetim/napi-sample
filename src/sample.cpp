#include "sample.h"

#include <iostream>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <numeric>

#include <boost/version.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>

std::string sample::hello()
{
    return "hello world!";
}

Napi::String sample::HelloWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    Napi::String returnValue = Napi::String::New(env, sample::hello());

    return returnValue;
}

int sample::addition(int a, int b) 
{
    return a + b;
}

Napi::Number sample::AdditionWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    if (info.Length() < 2 || !info[0].IsNumber() || !info[1].IsNumber()) {
        Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
    }

    Napi::Number first = info[0].As<Napi::Number>();
    Napi::Number second = info[1].As<Napi::Number>();

    int returnValue = sample::addition(first.Int32Value(), second.Int32Value());

    return Napi::Number::New(env, returnValue);
}

bool sample::isSame(const std::vector<bool>& data)
{
    std::copy(
        data.begin(),
        data.end(),
        std::ostream_iterator<bool>(std::cout, ", "));

    auto it = std::adjacent_find(
        data.begin(),
        data.end(),
        std::not_equal_to<int>());

    return it == data.end();
}

Napi::Boolean sample::IsSameWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsArray()) {
        Napi::TypeError::New(env, "Array expected").ThrowAsJavaScriptException();
    }

    Napi::Array first = info[0].As<Napi::Array>();
    
    std::vector<bool> firstData;

    for (int i = 0; i < static_cast<int>(first.Length()); i++) {
        firstData.push_back(first.Get(i).ToBoolean());
    }
    std::cout << "length " << first.Length() << "\n";

    bool returnValue = sample::isSame(firstData);

    return Napi::Boolean::New(env, returnValue);
}

int sample::total(const std::vector<int>& data)
{
    return std::accumulate(
        data.begin(),
        data.end(),
        0);
}

Napi::Number sample::TotalWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsArray()) {
        Napi::TypeError::New(env, "Array expected").ThrowAsJavaScriptException();
    }

    Napi::Array first = info[0].As<Napi::Array>();

    std::vector<int> data;
    for (int i = 0; i < static_cast<int>(first.Length()); i++) {
        data.push_back(first.Get(i).ToNumber());
    }

    int returnValue = sample::total(data);

    return Napi::Number::New(env, returnValue);
}

std::vector<double> sample::interpolator(const std::vector<double>& data)
{
    std::vector<double> result;

    result.insert(result.end(), data.begin(), data.end());
    result.insert(result.end(), data.begin(), data.end());

    return result;
}

Napi::Array sample::InterpolatorWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsArray()) {
        Napi::TypeError::New(env, "Array expected").ThrowAsJavaScriptException();
    }

    Napi::Array first = info[0].As<Napi::Array>();

    std::vector<double> data;
    for (int i = 0; i < static_cast<int>(first.Length()); i++) {
        data.push_back(first.Get(i).ToNumber());
    }

    std::vector<double> result = sample::interpolator(data);

    Napi::Array returnValue = Napi::Array::New(env, result.size());
    
    for (int i = 0; i < static_cast<int>(result.size()); i++) {
        returnValue.Set(i, result[i]);
    }

    return returnValue;
}

LaneNode sample::createLaneNode(const std::string& name, double x, double y)
{
    static int index = 0;

    LaneNode laneNode;
    laneNode.name = name;
    laneNode.index = index++;
    laneNode.maxSpeed_mps = 2;
    laneNode.x = x;
    laneNode.y = y;

    return laneNode;
}

Napi::Object sample::CreateLaneNodeWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 3
        || !info[0].IsString() 
        || !info[1].IsNumber() 
        || !info[2].IsNumber())
    {
        Napi::TypeError::New(env, "Array expected").ThrowAsJavaScriptException();
    }

    Napi::String fileName = info[0].As<Napi::String>();
    Napi::Number x = info[1].As<Napi::Number>();
    Napi::Number y = info[2].As<Napi::Number>();

    LaneNode laneNode = sample::createLaneNode(fileName, x, y);

    Napi::Object obj = Napi::Object::New(env);

    obj.Set(Napi::String::New(env, "name"), laneNode.name);
    obj.Set(Napi::String::New(env, "index"), laneNode.index);
    obj.Set(Napi::String::New(env, "x"), laneNode.x);
    obj.Set(Napi::String::New(env, "y"), laneNode.y);
    obj.Set(Napi::String::New(env, "maxSpeed_mps"), laneNode.maxSpeed_mps);

    return obj;
}

std::vector<LaneNode> sample::generateLaneNodes(
    std::vector<
        std::pair<double, double>
    > nodes)
{
    std::vector<LaneNode> laneNodes;
    int index = 0;

    std::transform(
        nodes.begin(),
        nodes.end(),
        std::back_inserter(laneNodes),
        [&index](const std::pair<double, double>& node) {
            double x = node.first;
            double y = node.second;

            LaneNode laneNode;

            laneNode.x = x;
            laneNode.y = y;
            laneNode.maxSpeed_mps = 3;
            laneNode.name = std::to_string(x) + ", " + std::to_string(y);
            laneNode.index = index++;

            return laneNode;
        });

    return laneNodes;
}

Napi::Array sample::GenerateLaneNodesWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1
        || !info[0].IsArray())
    {
        Napi::TypeError::New(env, "Array expected").ThrowAsJavaScriptException();
    }

    Napi::Array input = info[0].As<Napi::Array>();

    std::vector<
        std::pair<double, double>
    > nodes;

    for (int i = 0; i < static_cast<int>(input.Length()); i++) {
        Napi::Object object = input.Get(i).As<Napi::Object>();

        double x = 0;
        double y = 0;

        if (object.Has("x")) {
            x = object.Get("x").ToNumber();
        }

        if (object.Has("y")) {
            y = object.Get("y").ToNumber();
        }

        nodes.push_back(std::make_pair(x, y));
    }

    std::vector<LaneNode> result = sample::generateLaneNodes(nodes);

    Napi::Array returnValue = Napi::Array::New(env, result.size());
    int index = 0;

    std::for_each(
        result.begin(),
        result.end(),
        [&returnValue, &index, &env](const LaneNode& laneNode) {
            Napi::Object obj = Napi::Object::New(env);

            obj.Set(Napi::String::New(env, "name"), laneNode.name);
            obj.Set(Napi::String::New(env, "index"), laneNode.index);
            obj.Set(Napi::String::New(env, "x"), laneNode.x);
            obj.Set(Napi::String::New(env, "y"), laneNode.y);
            obj.Set(Napi::String::New(env, "maxSpeed_mps"), laneNode.maxSpeed_mps);

            returnValue.Set(index++, obj);
        });

    return returnValue;
}

std::string sample::getBoostVersion()
{
    std::ostringstream oss;

    boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
    boost::filesystem::path p{"/usr/include"};
    oss << p.root_name() << '\n'
        << p.root_directory() << '\n'
        << p.root_path() << '\n'
        << p.relative_path() << '\n'
        << p.parent_path() << '\n'
        << p.filename() << '\n'
        << "Current time "
        << now.time_of_day().hours() << ":" << now.time_of_day().minutes() 
        << "\n";

    oss << "Boost version: " 
        << BOOST_VERSION / 100000
        << "."
        << BOOST_VERSION / 100 % 1000
        << "."
        << BOOST_VERSION % 100
        << "\n";

    return oss.str();
}

Napi::String sample::GetBoostVersionWrapped(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    std::string result = sample::getBoostVersion();

    return Napi::String::New(env, result);
}

Napi::Object sample::Init(Napi::Env env, Napi::Object exports)
{
    exports.Set(
        "hello",
        Napi::Function::New(env, sample::HelloWrapped)
    );

    exports.Set(
        "addition",
        Napi::Function::New(env, sample::AdditionWrapped)
    );

    exports.Set(
        "isSame",
        Napi::Function::New(env, sample::IsSameWrapped)
    );

    exports.Set(
        "total",
        Napi::Function::New(env, sample::TotalWrapped)
    );

    exports.Set(
        "interpolator",
        Napi::Function::New(env, sample::InterpolatorWrapped)
    );

    exports.Set(
        "createLaneNode",
        Napi::Function::New(env, sample::CreateLaneNodeWrapped)
    );

    exports.Set(
        "generateLaneNodes",
        Napi::Function::New(env, sample::GenerateLaneNodesWrapped)
    );

    exports.Set(
        "getBoostVersion",
        Napi::Function::New(env, sample::GetBoostVersionWrapped)
    );

    return exports;
}