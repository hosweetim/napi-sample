#ifndef UTIL_WRAPPER_HPP
#define UTIL_WRAPPER_HPP

#include <napi.h>

#include "util.h"

class UtilWrapper: public Napi::ObjectWrap<UtilWrapper> {
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);
    UtilWrapper(const Napi::CallbackInfo& info);
    Util* GetInternalInstance();

private:
    static Napi::FunctionReference constructor;
    Napi::Value GetValue(const Napi::CallbackInfo& info);
    void Increment(const Napi::CallbackInfo& info);
    Util *util;
};

#endif // UTIL_WRAPPER_HPP
