#include "util.h"

Util::Util(int value):
    value(value)
{
}

int Util::getValue()
{
    return value;
}

void Util::increment() 
{
    value++;
}