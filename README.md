## Napi Sample

This is simple sample code on how to use `Napi`

There are 2 compilation methods

 - `node-gyp`
 - `cmake-js`

### Node Gyp

This requires the `binding.gyp` file and to invoke the compilation

```
node-gyp rebuild
```

### Cmake-JS

This method uses `CMakeLists.txt` file for the compilation information and to invoke the compilation

```
cmake-js rebuild
```

### References

 - https://medium.com/@atulanand94/beginners-guide-to-writing-nodejs-addons-using-c-and-n-api-node-addon-api-9b3b718a9a7f
 - https://github.com/nodejs/node-addon-api
 - https://www.npmjs.com/package/cmake-js